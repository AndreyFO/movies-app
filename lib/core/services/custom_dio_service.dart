import 'package:dio/dio.dart';
import 'package:movieapp/core/services/dio_service.dart';

class CustomDioService implements DioService {
  @override
  Dio getDio() {
    return Dio(
      BaseOptions(
        baseUrl: 'https://api.themoviedb.org/4/',
        headers: {
          'content-type': 'application/json;charset=utf-8',
          'authorization':
              'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwY2RhY2YxNGVjMTE3ZGQ5ODIxYmM1MTBjNjFlNmNlNyIsInN1YiI6IjYxOTdmYmZkNzE0NThmMDA2MTg5YTZiMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.9LgWyvz7NKWUM_e16nObhkQfyj80_7VbrFJ4T1ISRfw',
        },
      ),
    );
  }
}
