import 'package:flutter/material.dart';

class Genre {
  int id;
  String title;
  bool active;

  Genre(this.id, this.title, {this.active = false});

  void toggleActive() {
    active = !active;
  }
}

class Movie {
  List<int> genreIds;
  String name;

  Movie(this.name, this.genreIds);
}

class MovieGenerator {
  static Future<List<Movie>> getMovies() async {
    return [
      Movie("Dairy kid", [1, 2]),
      Movie("Dairy Man", [1]),
    ];
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => HomeState();
}

class HomeState extends State<Home> {
  late List<Genre> genres;

  @override
  void initState() {
    super.initState();
    genres = [
      Genre(1, "Ação", active: true),
      Genre(2, "Aventura"),
      Genre(3, "Fntasia"),
      Genre(4, "Comédia"),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(genres.length, (i) {
              var cur = genres[i];
              return Chip(
                  title: cur.title,
                  active: cur.active,
                  onTap: () {
                    setState(() {
                      genres[i].toggleActive();
                    });
                  });
            }),
          ),
        ),
        FutureBuilder<List<Movie>>(
            future: MovieGenerator.getMovies(),
            builder: (c, snapshot) {
              if (snapshot.hasData) {
                var movies = snapshot.data;
                var selectedGenres =
                    genres.where((e) => e.active).map((e) => e.id);
                var filteredMovies = movies?.where((a) {
                  bool shouldAdd = false;
                  for (int i = 0; i < a.genreIds.length; i++) {
                    if (selectedGenres.contains(a.genreIds[i])) {
                      shouldAdd = true;
                      break;
                    }
                  }

                  return shouldAdd;
                }).toList();
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(filteredMovies!.length,
                      (i) => Text(filteredMovies[i].name)),
                );
              } else {
                return Container();
              }
            }),
      ]),
    );
  }
}

class Chip extends StatelessWidget {
  final String title;
  final bool active;
  final Function() onTap;

  const Chip(
      {Key? key,
      required this.title,
      required this.active,
      required this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          padding: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(999.0),
            color: active ? Colors.red : Colors.grey,
          ),
          child: Text(title,
              style: TextStyle(
                color: active ? Colors.white : Colors.black,
              ))),
    );
  }
}
