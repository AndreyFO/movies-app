import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:movieapp/core/utils/custom_colors.dart';

class TextStyles {
  static final titleHome = GoogleFonts.montserrat(
    fontSize: 22,
    fontWeight: FontWeight.w600,
    color: CColors.grayHigh,
  );
  static final titleRegular = GoogleFonts.montserrat(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: CColors.titleGenre,
  );
  static final titleRegularGray = GoogleFonts.montserrat(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: CColors.grayHigh,
  );

  static final titleSmall = GoogleFonts.montserrat(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: CColors.titleGenre,
  );
  static final titleSmallGray = GoogleFonts.montserrat(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: CColors.grayLight,
  );
  static final averageHigh = GoogleFonts.montserrat(
    fontSize: 24,
    fontWeight: FontWeight.w600,
    color: CColors.averageHigh,
  );
  static final averageLow = GoogleFonts.montserrat(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: CColors.grayNormal,
  );
  static final infoLow = GoogleFonts.montserrat(
    fontSize: 12,
    fontWeight: FontWeight.w600,
    color: CColors.grayNormal,
  );
  static final infoHigh = GoogleFonts.montserrat(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: CColors.grayHigh,
  );
  static final backText = GoogleFonts.montserrat(
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: CColors.grayArrow,
  );
  static final defaultFont = GoogleFonts.montserrat(
    fontSize: 12,
    fontWeight: FontWeight.w400,
  );
  static final searchText = GoogleFonts.montserrat(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: CColors.grayLight,
  );
}
