import 'package:flutter/material.dart';

abstract class CColors {
  static const grayLight = Color(0xFF5E6770);
  static const grayNormal = Color(0xFF868E96);
  static const grayHigh = Color(0xFF343A40);
  static const grayArrow = Color(0xFF6D7070);

  static const titleGenre = Color(0xFFFFFFFF);
  static const averageHigh = Color(0xFF00384C);
  static const background = Color(0xFFF5F5F5);
  static const backgroundBox = Color(0xFFF1F3F5);
  static const shdowBackround = Color(0xFFFAFAFA);
}
