import 'package:movieapp/core/services/dio_service.dart';
import 'package:movieapp/core/utils/api_request.dart';

import 'package:movieapp/models/movie_details.dart';
import 'package:movieapp/models/movie_list.dart';

import 'package:movieapp/repositories/movies_repository_interface.dart';

class MoviesRepository implements IMoviesRespository {
  final DioService _dioService;

  MoviesRepository(this._dioService);

  @override
  Future<Movies> getMovies() async {
    var result = await _dioService.getDio().get(API.REQUEST_MOVIE_LIST);
    return Movies.fromJson(result.data);
  }

  @override
  Future<MovieDetail> fetchMovieById(int id) async {
    var result = await _dioService.getDio().get(API.REQUES_DETAILS(id));
    return MovieDetail.fromJson(result.data);
  }
}
