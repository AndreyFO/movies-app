import 'package:movieapp/models/movie_details.dart';
import 'package:movieapp/models/movie_list.dart';

abstract class IMoviesRespository {
  Future<Movies> getMovies();
  Future<MovieDetail> fetchMovieById(int id);
}
