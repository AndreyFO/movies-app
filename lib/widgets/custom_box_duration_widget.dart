import 'package:flutter/material.dart';
import 'package:movieapp/core/utils/custom_colors.dart';
import 'package:movieapp/core/utils/custom_theme.dart';

class CustomBoxDurationWidget extends StatelessWidget {
  final String text;

  final int runtime;

  final RichText richText;

  const CustomBoxDurationWidget({
    Key? key,
    required this.text,
    required this.richText,
    required this.runtime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String stringRuntime = "$runtime";

    return Container(
        decoration: const BoxDecoration(
          color: CColors.backgroundBox,
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: RichText(
            text: TextSpan(
              text: text,
              style: TextStyles.infoLow,
              children: <TextSpan>[
                TextSpan(text: stringRuntime, style: TextStyles.infoHigh),
              ],
            ),
          ),
        ));
  }
}
