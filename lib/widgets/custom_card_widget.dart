import 'package:flutter/material.dart';

import 'package:movieapp/core/utils/api_request.dart';
import 'package:movieapp/core/utils/custom_theme.dart';

import 'package:movieapp/models/movie_list.dart';

import 'package:movieapp/pages/details_page.dart';

class CustomCardWidget extends StatelessWidget {
  final Movie movie;
  final Movies moviedetail;

  const CustomCardWidget({
    Key? key,
    required this.movie,
    required this.moviedetail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => DetailsPage(
                moviedetail: moviedetail,
                movie: movie,
              ),
              fullscreenDialog: true,
            ),
          );
        },
        child: Stack(children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.network(
              API.REQUEST_IMG(movie.posterPath),
            ),
          ),
          Container(
              height: 505,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10)),
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.center,
                  colors: [Colors.black, Colors.transparent],
                ),
              )),
          Positioned(
            bottom: 56,
            left: 24,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(
                movie.title.toUpperCase(),
                style: TextStyles.titleSmall,
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
