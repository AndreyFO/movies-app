import 'package:flutter/material.dart';
import 'package:movieapp/models/movie_details.dart';

import 'package:movieapp/repositories/movies_repository.dart';

class MovieController {
  final MoviesRepository _moviesRepository;
  MovieController(this._moviesRepository) {
    fetch();
  }

  ValueNotifier<MovieDetail?> moviesdetails = ValueNotifier<MovieDetail?>(null);

  fetch() async {
    moviesdetails.value = await _moviesRepository.fetchMovieById(3);
  }
}
