import 'package:flutter/material.dart';

import 'package:movieapp/controllers/movie_controller.dart';
import 'package:movieapp/core/services/custom_dio_service.dart';

import 'package:movieapp/core/utils/custom_colors.dart';
import 'package:movieapp/core/utils/custom_theme.dart';
import 'package:movieapp/models/movie_list.dart';

import 'package:movieapp/repositories/movies_repository.dart';
import 'package:movieapp/widgets/custom_card_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class Genre {
  int id;
  String title;
  bool active;

  Genre(this.id, this.title, {this.active = false});

  void toggleActive() {
    active = !active;
  }
}

class Chip extends StatelessWidget {
  final String title;
  final bool active;
  final Function() onTap;

  const Chip(
      {Key? key,
      required this.title,
      required this.active,
      required this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: CColors.backgroundBox,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(200.0),
            color: active ? CColors.averageHigh : Colors.white,
          ),
          child: Text(title,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontFamily: "Montserrat",
                fontSize: 12,
                color: active ? Colors.white : CColors.averageHigh,
              ))),
    );
  }
}

class _HomePageState extends State<HomePage> {
  final MovieController _controller = MovieController(
    MoviesRepository(CustomDioService()),
  );
  late List<Genre> genres;

  @override
  void initState() {
    super.initState();
    genres = [
      Genre(28, "Ação", active: true),
      Genre(12, "Aventura"),
      Genre(14, "Fantasia"),
      Genre(35, "Comédia"),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Padding(
              padding: const EdgeInsets.all(28),
              child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Filmes',
                        style: TextStyles.titleHome,
                      ),
                      const SizedBox(height: 24),
                      Container(
                        decoration: BoxDecoration(
                            color: CColors.backgroundBox,
                            borderRadius: BorderRadius.circular(100)),
                        child: TextField(
                            decoration: InputDecoration(
                              fillColor: CColors.backgroundBox,
                              prefixIcon: const Icon(
                                Icons.search,
                                color: CColors.grayHigh,
                              ),
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              hintText: 'Pesquise filmes',
                              hintStyle: TextStyles.searchText,
                            ),
                            onChanged: _controller.onChanged),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: List.generate(genres.length, (i) {
                          var cur = genres[i];
                          return Chip(
                              title: cur.title,
                              active: cur.active,
                              onTap: () {
                                setState(() {
                                  genres[i].toggleActive();
                                });
                              });
                        }),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      const SizedBox(height: 15),
                      ValueListenableBuilder<Movies?>(
                          valueListenable: _controller.movies,
                          builder: (_, movies, ___) {
                            return movies != null
                                ? ListView.separated(
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: movies.listMovies.length,
                                    itemBuilder: (_, index) => CustomCardWidget(
                                      moviedetail: movies,
                                      movie: movies.listMovies[index],
                                    ),
                                    separatorBuilder: (_, __) => const SizedBox(
                                      height: 16,
                                    ),
                                  )
                                : Container();
                          }),
                      FutureBuilder<List<Movie>>(builder: (c, snapshot) {
                        if (snapshot.hasData) {
                          var movies = snapshot.data;
                          var selectedGenres =
                              genres.where((e) => e.active).map((e) => e.id);
                          var filteredMovies = movies?.where((a) {
                            bool shouldAdd = false;
                            for (int i = 0; i < a.genreIds.length; i++) {
                              if (selectedGenres.contains(a.genreIds[i])) {
                                shouldAdd = true;
                                break;
                              }
                            }

                            return shouldAdd;
                          }).toList();
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: List.generate(filteredMovies!.length,
                                (i) => Text(filteredMovies[i].title)),
                          );
                        } else {
                          return Container();
                        }
                      }),
                    ]),
              ))),
    );
  }
}
