import 'package:flutter/material.dart';

import 'package:movieapp/core/utils/api_request.dart';
import 'package:movieapp/core/utils/custom_colors.dart';
import 'package:movieapp/core/utils/custom_theme.dart';

import 'package:movieapp/models/movie_list.dart';

import 'package:movieapp/pages/home_page.dart';
import 'package:movieapp/widgets/custom_box_duration_widget.dart';
import 'package:movieapp/widgets/custom_box_widget.dart';

class DetailsPage extends StatelessWidget {
  final Movie movie;
  final Movies moviedetail;

  const DetailsPage({
    Key? key,
    required this.movie,
    required this.moviedetail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 48, left: 20),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: 32,
                  decoration: BoxDecoration(
                    color: CColors.titleGenre,
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black26,
                          blurRadius: 2,
                          spreadRadius: 0.05,
                          offset: Offset(0, 3))
                    ],
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        iconSize: 16,
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: CColors.grayArrow,
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const HomePage()));
                        },
                      ),
                      Text(
                        'Voltar',
                        textAlign: TextAlign.start,
                        style: TextStyles.backText,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 65,
                ),
                child: Center(
                  child: Column(children: [
                    SizedBox(
                      height: 318,
                      width: 216,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          API.REQUEST_IMG(movie.posterPath),
                        ),
                      ),
                    ),
                    const SizedBox(height: 32),
                    RichText(
                      text: TextSpan(
                          text: movie.voteAverage.toString(),
                          style: TextStyles.averageHigh,
                          children: <TextSpan>[
                            TextSpan(
                              text: ' / 10',
                              style: TextStyles.averageLow,
                            )
                          ]),
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    Text(
                      movie.title.toUpperCase(),
                      style: TextStyles.titleRegularGray,
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text(
                      'Título original: ' + movie.originalTitle,
                      style: TextStyles.titleSmallGray,
                    ),
                    const SizedBox(height: 32),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 44),
                              child: CustomBoxWidget(
                                text: 'Ano:  ',
                                releasedate: movie.releaseDate,
                                runtime: 0,
                                richText: RichText(
                                  text: const TextSpan(),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                margin:
                                    const EdgeInsets.only(left: 12, right: 43),
                                child: CustomBoxDurationWidget(
                                  text: 'Duração:  ',
                                  runtime: moviedetail.runtime,
                                  richText: RichText(
                                    text: const TextSpan(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ]),
                ),
              ),
              const SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Descrição', style: TextStyles.searchText),
                      Text(
                        movie.overview,
                        style: TextStyles.titleRegularGray,
                      )
                    ]),
              ),
              const SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.90,
                  child: CustomBoxDurationWidget(
                    text: 'ORÇAMENTO:  ',
                    runtime: 152000000,
                    richText: RichText(
                      text: const TextSpan(),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 4),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.90,
                  child: CustomBoxWidget(
                    text: 'PRODUTORAS:  ',
                    releasedate: 'Marvel Studios',
                    runtime: 0,
                    richText: RichText(
                      text: const TextSpan(),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
