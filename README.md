# movieapp

Projeto App Movies Flutter

## Getting Started

Projeto flutter realizado com príncipio para busca de filmes e detalhes atráves da API TMDB.

## Observações

Realizei a consutrução com base nos meus conhecimentos e pesquisas na internet.

Tive dificuldade com a comunicação da API Versão 3, que contém o endpoint de "Detalhes do Filme".
Sendo assim, consumi a Versão 4 da API e endpoint de Detalhes do Filme é diferente, fazendo com que algumas informações não fossem possiveis de acesar:
Genero, diretor, orçamento, elenco, duração.

Com isso, apenas deixei o layout desenhado e mockei algumas informações apenas para exemplo.

Ressaltando também, como são apenas 6 meses de experiencia em flutter/dart, 
tentei aplicar uma logica para filtrar os filmes na home page por Generos em suas respectivas tabs, porém sem sucesso.

Foi uma experiência ótima, muito aprendizado, alguns dias de muito esforço e dedicação e mesmo com essa entrega, continuarei tentado completar de fato o projeto.

## Agradecimentos

Desde já, agradeço a oportunidade à Cubos.


For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
